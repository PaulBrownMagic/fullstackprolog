:- module(pengines_server,
    [ serve/1
    , serve/0
    ]).

% Web Server
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).

serve :-
    serve(5000).

serve(Port) :-
    http_server(http_dispatch, [port(Port)]).

% File paths
:- dynamic user:file_search_path/2.
:- prolog_load_context(directory, Dir),
   asserta(user:file_search_path(app, Dir)).

% Pengines: register app
:- use_module(library(pengines)).
:- pengine_application(lines).
:- use_module(lines:app(pengine_apps/lines)).

% Webpage served and static handling
:- use_module(app(www/webpage)).
