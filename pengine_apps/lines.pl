:- module(lines,
    [ vertical/1
    , horizontal/1
    ]
).

vertical(line(point(X,_),point(X,_))).

horizontal(line(point(_,Y),point(_,Y))).
