:- module(webpage, []).

:- use_module(library(http/html_write)).
:- use_module(library(http/http_files)).

http:location(static, '/static', []).

:- http_handler(static(.), http_reply_from_files(app(www/static), []), [prefix]).

:- http_handler(root(.), main_page, []).

main_page(_Request) :-
    reply_html_page(
        base_style,
        [ title("FullStackProlog")
        ],
        [ h1("Hello, full stack Prolog!")
        , p("Better open the terminal...")
        , p("On page load Tau-Prolog will query a pengine for `horizontal(line(point(2, 3), point(4, Y))` and print the result in the console.")
        , p("Simultaneously it'll be querying `member(X, [a, b, c])` and printing those results to the console.")
        , p("This is a very, very early prototype that's full of bugs and limited functionality. But, we can query a pengine from Tau-Prolog.")
        ]
    ).

body(base_style, Body) -->
    html(body([ div(Body)
        , script(src('/static/js/tau-prolog.js'), [])
        , script(src('/static/js/jquery-3.4.1.min.js'), [])
        , script(src('/static/js/pengines.js'), [])
        , script(src('/static/js/fullstack.js'), [])
        ])
    ).
