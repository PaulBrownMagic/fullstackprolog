session = pl.create(1000);
window.pengines = new PengineMaster()
$.ready(pengines.session_init())

function PengineMaster() {

    this.live_pengines = {};

    this.session_init = function() {
        // Load the Prolog file and query init.
        $.get("/static/pl/fullstack.pl", function(data) {
                const parsed = session.consult(data);
                 if (parsed !== true) { console.log(pl.format_answer(parsed)) }
                 const query = session.query("init.")
                 if (query !== true) { console.log(pl.format_answer(query)) }
                 session.answer((a) => {})
            });
    };
    this.session_send = function (query) {
        session.query(query)
        session.answer(()=>{})
    };
    this.new_pengine = function (query, opts) {
        const session_send = this.session_send
        const live_pengines = this.live_pengines
        return new Pengine({
                onsuccess: function () {
                    const more = this.more ? "more" : "none"
                    // tmp replace vars until prolog can be returned
                    for (const d of this.data) {
                        let res = this.pengine.options.ask
                        $.each(d, (k, v) => {
                            res = res.replace(new RegExp(k, "g"), v)
                        })
                        // Send to Tau, as a compound term
                        session_send(`pengine_receive('${this.id}', ${more}, ${res}).`)
                    }
                },
                onfailure: function () {
                    const query = this.pengine.options.ask
                    session_send(`pengine_failed('${this.id}', ${query}).`)
                },
                ask: query,
                oncreate: function () {
                    live_pengines[this.id] = this.pengine
                },
                ...opts,
            })
    };
    this.ask = function (query, options) {
         const opts = JSON.parse(options)
         this.new_pengine(query, opts)
    };
    this.next = function (id) {
        this.live_pengines[id].next()
    };
    this.destroy = function (id) {
        this.live_pengines[id].destroy()
        delete this.live_pengines[id]
    }
}
