:- use_module(library(js)).

init :-
    pengine_ask('member(X, [a, b, c])'),
    pengine_ask('horizontal(line(point(2, 3), point(4, Y)))').


pengine_call(N, Args) :-
    prop(pengines, P),
    prop(P, N, F),
    apply(P, F, Args, _).

pengine_ask(Query) :-
    pengine_call(ask, [Query, '{"chunk": 3, "application": "lines"}']).

pengine_next(ID) :-
    pengine_call(next, [ID]).

pengine_destroy(ID) :-
    pengine_call(destroy, [ID]).


pengine_failed(ID, Query) :-
    write('Failed'), write(ID), writeq(Query).

% Iterate through answers
pengine_receive(ID, more, member(X, L)) :-
    write(X),
    pengine_next(ID).
pengine_receive(ID, none, member(X, L)) :-
    write(X),
    pengine_destroy(ID).

% Take first answer
pengine_receive(ID, _, horizontal(line(point(X1, Y1), point(X2, Y2)))) :-
    write([X1, Y1, X2, Y2]), pengine_destroy(ID).
